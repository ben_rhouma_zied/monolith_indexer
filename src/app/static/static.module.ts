import { NgModule } from '@angular/core';

import { SharedModule } from '@app/shared';

import { StaticRoutingModule } from './static-routing.module';
import { IndexComponent } from './index/index.component';
import { FeaturesComponent } from './features/features.component';

@NgModule({
  imports: [SharedModule, StaticRoutingModule],
  declarations: [IndexComponent, FeaturesComponent]
})
export class StaticModule {}
