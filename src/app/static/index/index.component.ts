import { Component, OnInit, AfterContentInit } from '@angular/core';

import { ROUTE_ANIMATIONS_ELEMENTS } from '@app/core';
// import * as Paralax from 'paralax-js';
@Component({
  selector: 'anms-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.scss']
})
export class IndexComponent implements OnInit, AfterContentInit {
  routeAnimationsElements = ROUTE_ANIMATIONS_ELEMENTS;

  constructor() {}

  ngOnInit() {}

  ngAfterContentInit() {
    // const scene = document.getElementById('wrapper')
    // const parallaxInstance = new Paralax(scene ,{
    //   relativeInput: true,
    //   hoverOnly: true
    // });
  }
}
